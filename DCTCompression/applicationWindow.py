import sys
from PyQt5.QtCore import QFile
from PyQt5.QtWidgets import QApplication, QWidget, QDialog, QFileDialog, QAction
from PyQt5.QtGui import QIcon, QIntValidator
from PyQt5 import uic

class startDialog(QDialog):
	
	def __init__(self):

		super().__init__()

		self.imagePath = ''
		self.qualityValue = 0
		self.blockDimension = 1

		self.initUi()

	def initUi(self):		
		#Setting the ui file
		self.ui = uic.loadUi('dialogtest.ui',self)
		self.ui.setWindowTitle('DCT2 compression')

		#LineEdit settings
		self.ui.dimensionEdit.setValidator(QIntValidator()	)

		#Buttons settings
		self.ui.selectImageButton.setToolTip('Press to select BMP image path.')		
		self.ui.selectImageButton.clicked.connect(self.imageButtonClicked)

		self.ui.startButton.setToolTip('Press for JPEG conversion.')
		self.ui.startButton.clicked.connect(self.startButtonClicked)

		#Event settings
		self.ui.qualitySlider.valueChanged.connect(self.sliderValueChange)
		
		self.show()

	def sliderValueChange(self):
		self.qualityValue = str(self.ui.qualitySlider.value())
		self.ui.qualityEdit.setText(self.qualityValue)

	def imageButtonClicked(self):
		self.imagePath = QFileDialog.getOpenFileName(self, "Choose a bmp file",'', '*.bmp')[0]
		self.ui.imageEdit.setText(self.imagePath)

	def startButtonClicked(self):
		self.blockDimension = self.ui.dimensionEdit.text()
		print('Blocco: ', self.blockDimension, '\nPath: ', self.imagePath, '\nQuality: ', self.qualityValue)
