from numpy import array, arange
from dctalg import *
import numpy as np

def printMatr(matr):
	for r in matr:
		for i in r:
			print ("%d\t" % i, end="")
		print ('')

def aprint(name, array):
	print("%s\t" % name, array)
	print("Rows: %d\tCols: %d\n" % (len(array), len(array[0])))

TEST_MATRIX_DCT = array([[231, 32, 233, 161, 24, 71, 140, 245],
				   [247, 40, 248, 245, 124, 204, 36, 107],
				   [234, 202, 245, 167, 9, 217, 239, 173],
				   [193, 190, 100, 167, 43, 180, 8, 70],
				   [11, 24, 210, 177, 81, 243, 8, 112],
				   [97, 195, 203, 47, 125, 114, 165, 181],
				   [193, 70, 174, 167, 41, 30, 127, 245],
				   [87, 149, 57, 192, 65, 129, 178, 228]], dtype = np.float)

from inspect import *
class TestSuite:
	executedTests = 0
	passedTests = 0



	def executeTests(self):
		self.testCase1('./big_tree.bmp')
		self.testCase1('./big_tree.bmp', 2)
		self.testCase1('./big_tree.bmp', 4)

		self.testCase2('./big_tree.bmp', './big_tree_q50.bmp', 50, 1)
		self.testCase2('./big_tree.bmp', './big_tree_q10.bmp', 10, 3)
		self.testCase2('./big_tree.bmp', './big_tree_q100.bmp', 100, 2)
		self.testCase2('./big_tree.bmp', './big_tree_q33.bmp', 33, 7)

		print("\nTotal executed tests\t%d\nTotal passed tests:\t%d\n" % (self.executedTests, self.passedTests))

	def testCase1(self, imagePath, n=1):
		N = 8*n
		print("Testing: Complemento dimensione matrice con immagini reali.")
		print("Image: %s \nN: %d" % (imagePath, N))
		self.executedTests = self.executedTests + 1

		###
		o = DCTAlg(imagePath, 50, n)
		complImgMat = o.matrixComplementation()
		if ((len(complImgMat) % N) == 0) and ((len(complImgMat[0]) % N == 0)):
			self.passedTests = self.passedTests + 1
			print("Test superato.")
		else:
			print("Test NON SUPERATO.")

	def testCase2(self, imagePath, outImagePath, quality, n=1):
		N = 8*n
		print("Testing: Compressione di un'immagine.")
		print("Image: %s \nN: %d" % (imagePath, N))
		self.executedTests = self.executedTests + 1

		###
		o = DCTAlg(imagePath, quality, n)
		o.compress(outImagePath)
		self.passedTests += 1
