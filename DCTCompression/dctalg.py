import copy, PIL, time
from scipy import misc
from scipy.fftpack import dct, idct
from numpy import *

class DCTAlg:
	
	defaultQuantizationMatrix = array([[16, 11, 10, 16, 24, 40, 51, 61],
				[12, 12, 14, 19, 26, 58, 60, 55],
				[14, 13, 16, 24, 40, 57, 69, 56],
				[14, 17, 22, 29, 51, 87, 80, 62],
				[18, 22, 37, 56, 68, 109, 103, 77],
				[24, 35, 55, 64, 81, 104, 113, 92],
				[49, 64, 78, 87, 103, 121, 120, 101],
				[72, 92, 95, 98, 112, 100, 103, 99]])

	originalH = -1 # Altezza originale dell'immagine
	originalW = -1 # Larghezza originale dell'immagine

	imageMatrix     = array([]) # Matrice dell'immagine da elaborare
	N               = -1		# Dimensione dei blocchi (gia' multiplo di 8)
	quality			= -1		# Qualità utilizzata per la compressione
	originalImgPath = ''	# Percorso dell'immagine originale

	def __init__(self, imagePath, quality, n = 1):
		"""
		Inizializza l'oggetto settando l'immagine con cui lavorare e la dimensione dei blocchetti

		imagePath - string
			Il percorso dell'immagine da elaborare.
		n - integer, optional
			Il multiplo di 8 da utilizzare per maggiorare i quadrati 8x8. (default 1)
		"""
		if imagePath != '' and n > 0:
			self.N = n * 8
			self.getImageMatrix(imagePath)
			self.quality = quality
			self.originalImgPath = imagePath
		else:
			raise ValueError('Constructor arguments are not valid.')

	def getImageMatrix(self, imagePath):
		"""
		Ritorna la matrice relativa all'immagine ricevuta in input.
		
		imagePath - string
			Il percorso dell'immagine da cui ricavare la matrice di interi.
		"""
		if imagePath !='':
			self.imageMatrix = misc.imread(imagePath, mode='L')
			self.originalW = len(self.imageMatrix[0])
			self.originalH = len(self.imageMatrix)
			return self.imageMatrix

		return array([])

	def matrixComplementation(self):
		"""
		Completa la matrice dell'immagine con dimensioni multiple di 8 * n.

		n - integer, optional
			Il multiplo di 8 da utilizzare per maggiorare i quadrati 8x8. (default 1)
		"""
		pluscol = self.originalW % self.N
		plusrow = self.originalH % self.N
		
		##### NON TOCCARE O SALTA TUTTO :) #####

		# Ripete l'ultimo pixel di ciasuna riga
		if plusrow != 0:
			for x in range(self.originalH, self.originalH + self.N - plusrow):
				#imageMatrix = append(imageMatrix, zeros(originalW).reshape(1, originalH), axis=0)
				self.imageMatrix = insert(self.imageMatrix, x, 0, axis=0)
				self.imageMatrix[x: , 0:len(self.imageMatrix[0])] = self.imageMatrix[self.originalH - 1:self.originalH].reshape(1, len(self.imageMatrix[0]))

		# Ripete l'ultima colonna
		if pluscol != 0:
			for x in range(self.originalW, self.originalW + self.N - pluscol):
				self.imageMatrix = insert(self.imageMatrix, x, 0, axis=1)
				self.imageMatrix[0:len(self.imageMatrix), x:] = self.imageMatrix[0:len(self.imageMatrix),self.originalW - 1:self.originalW].reshape(len(self.imageMatrix), 1)

		# [INUTILE] Ripete il pixel in basso a destra per completare la matrice
		#self.imageMatrix[self.originalH: , self.originalW:].fill(self.imageMatrix[self.originalH-1, self.originalW])

		return self.imageMatrix

	def dctApply(self):
		"""
		Applica la DCT ai sottoblocchi di imageMatrix di dimensione 8 * n.
		"""
		N = self.N; # Accorcia le istruzioni
		# Per ogni blocco calcola la DCT sulle righe, poi trasponi il blocco e ricalcola la DCT
		# in modo tale che il calcolo venga fatto sulle colonne.
		for i in range(0, len(self.imageMatrix), N): # Iterano i quadratini verticalmente
			for j in range(0, len(self.imageMatrix[0]), N): # Iterano i quadratini orrizzontalmente
				self.imageMatrix[i:i+N,j:j+N] = dct(self.imageMatrix[i:i+N,j:j+N], type=2, norm="ortho")
				self.imageMatrix[i:i+N,j:j+N] = dct(self.imageMatrix[i:i+N,j:j+N].T, type=2, norm="ortho").T

		return self.imageMatrix

	def getExpandedQuantMatrix(self, quantizationMatrix):
		"""
		Ritorna la matrice di quantizzazione di JPEG espansa per poter essere applicata all'immagine con blocchi di dimensione N*8.
		"""
		newQuantMatrix = quantizationMatrix.copy() # Crea una copia della matrice di quantizzazione di default
		
		N = self.N // 8
		# Duplica le righe
		for currentLine in range(0, len(newQuantMatrix) * N, N):
			for x in range(currentLine, currentLine + N - 1):
				newQuantMatrix = insert(newQuantMatrix, x + 1, 0, axis=0)
				newQuantMatrix[x+1, 0:len(newQuantMatrix[0])] = newQuantMatrix[currentLine, 0:len(newQuantMatrix[0])]
		
		# Duplica le colonne
		for currentCol in range(0, len(newQuantMatrix[0]) * N, N):
			for x in range(currentCol, currentCol + N - 1):
				newQuantMatrix = insert(newQuantMatrix, x + 1, 0, axis=1)
				newQuantMatrix[0:len(newQuantMatrix), x + 1] = newQuantMatrix[0:len(newQuantMatrix), currentCol]

		return newQuantMatrix

	def adjustQuantMatrix(self, quality):
		"""
		Calcola la matrice di quantizzazione in base alla qualita' in input.

		quality - integer
			Quality factor che verra' utilizzato per la compressione dell'immagine.
		"""
		# Copia la matrice di quantizzazione per mantenerla inalterata 
		qn = self.defaultQuantizationMatrix.copy()
		
		# controlli di sicurezza
		if quality <= 0:
			quality = 1

		# settaggio standard di qf

		if quality >= 100:
			for i in range(0, len(qn)):
				for j in range(0, len(qn[0])):
					 qn[i][j]= 1
		else:
			if quality < 50:
				qf = 50 / quality
			else:
				qf = (100 - quality) / 50 

			for i in range(0, len(qn)):
				for j in range(0, len(qn[0])):
					 qn[i][j]= round(qf * qn[i][j])

		return qn

	def quantization(self):
		"""
		Calcola la matrice Q in rapporto all'indice di qualita' e quantizza ogni blocco 8Nx8N dell'immagine.
		"""
		# Possono essere calcolati in parallelo
		quantMatrix = array(self.adjustQuantMatrix(self.quality))
		quantMatrix = self.getExpandedQuantMatrix(quantMatrix)

		cols = len(self.imageMatrix[0])
		rows = len(self.imageMatrix)

		for i in range(0, rows, self.N):
			for j in range(0, cols, self.N):
				for k in range(0, self.N):
					for h in range(0, self.N):
						#self.imageMatrix[i+k, j+h] = round(self.imageMatrix[i+k, j+h] / quantMatrix[k, h])*quantMatrix[k,h]
						self.imageMatrix[i+k, j+h] = self.imageMatrix[i+k, j+h] / quantMatrix[k, h]
		
		self.imageMatrix = around(self.imageMatrix) # Arrotonda
		for i in range(0, rows, self.N):
			for j in range(0, cols, self.N):
				for k in range(0, self.N):
					for h in range(0, self.N):
						self.imageMatrix[i+k, j+h] = self.imageMatrix[i+k, j+h] * quantMatrix[k,h]
		
		return self.imageMatrix

	def idctApply(self):
		"""
		Applica la DCT inversa ai sottoblocchi di imageMatrix di dimensione 8 * n.
		"""
		N = self.N
		for i in range(0, len(self.imageMatrix), N): # Iterano i quadratini verticalmente
			for j in range(0, len(self.imageMatrix[0]), N): # Iterano i quadratini orrizzontalmente
				self.imageMatrix[i:i+N,j:j+N] = idct(self.imageMatrix[i:i+N,j:j+N], type=2, norm="ortho")
				self.imageMatrix[i:i+N,j:j+N] = idct(self.imageMatrix[i:i+N,j:j+N].T, type=2, norm="ortho").T

		return self.imageMatrix

	def normalizeImageMatrix(self):
		"""
		Modifica la matrice dell'immagine in modo da avere valori compresi tra 0 e 255.
		"""
		for i in range(0, len(self.imageMatrix)):
			for j in range(0, len(self.imageMatrix[0])):
				if self.imageMatrix[i][j] < 0:
					self.imageMatrix[i][j] = 0
				elif self.imageMatrix[i][j] > 255:
					self.imageMatrix[i][j] = 255

		return self.imageMatrix
	
	def callsurrogate(val):
		"""
		Callbacksurrogate for tests
		"""
		return 0

	def compress(self, outImagePath, callbackProgress=callsurrogate):
		"""
		Applica tutti i passi dell'algoritmo di compressione e decompressione

		outImagePath - string
			(Non utilizzato) Percorso dell'immagine di output
		callbackProgress - callback function
			Funzione di callback per permettere di monitorare il procedimento
		
		Ritorna un oggetto PIL.Image.
		"""
		callbackProgress(5)
		self.imageMatrix = self.imageMatrix.astype('double')
		callbackProgress(5)
		self.imageMatrix = self.imageMatrix - 128
		start = time.clock()
		callbackProgress(5)
		self.matrixComplementation()
		callbackProgress(15)
		print('matrixComplementation() computation time:\t', time.clock() - start)
		start = time.clock()
		self.dctApply()
		print('dctApply() computation time:\t', time.clock() - start)
		callbackProgress(20)
		start = time.clock()
		self.quantization()
		print('quantization() computation time:\t', time.clock() - start)
		callbackProgress(35)
		start = time.clock()
		self.idctApply()
		print('idctApply() computation time:\t', time.clock() - start)
		callbackProgress(16)
		self.imageMatrix = around(self.imageMatrix + 128)
		self.imageMatrix = self.imageMatrix.astype('int32')
		im = PIL.Image.fromarray(self.imageMatrix[0:self.originalH, 0:self.originalW])
		return im