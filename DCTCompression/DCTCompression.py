#!/usr/bin/python

import sys, getopt, asyncio, tempfile
from dctalg import *
from utils import *
from numpy import *
from scipy import *


from PyQt5.QtCore import QFile
from PyQt5.QtWidgets import QApplication, QWidget, QDialog, QFileDialog, QAction
from PyQt5.QtGui import QIcon, QIntValidator
from PyQt5 import uic
#import matplotlib.pyplot as plt
import matplotlib.image as mpimg


################################ Start Window #########################################

class startDialog(QDialog):
	
	def __init__(self):

		super().__init__()

		self.imagePath = ''
		self.qualityValue = 0
		self.blockDimension = 1
		self.progressBarValue = 0

		self.initUi()

	def initUi(self):		
		#Setting the ui file
		self.ui = uic.loadUi('startDialogGUI.ui',self)
		self.ui.setWindowTitle('DCT2 compression')

		#LineEdit settings
		self.ui.dimensionEdit.setValidator(QIntValidator()	)

		#Buttons settings
		self.ui.selectImageButton.setToolTip('Press to select BMP image path.')		
		self.ui.selectImageButton.clicked.connect(self.imageButtonClicked)

		self.ui.startButton.setToolTip('Press for DCT2 compression.')
		self.ui.startButton.clicked.connect(self.startButtonClicked)

		#Event settings
		self.ui.qualitySlider.valueChanged.connect(self.sliderValueChange)
		
		self.show()

	def sliderValueChange(self):
		self.qualityValue = str(self.ui.qualitySlider.value())
		self.ui.qualityEdit.setText(self.qualityValue)

	def imageButtonClicked(self):
		self.resetProgressBar()
		self.imagePath = QFileDialog.getOpenFileName(self, "Choose a bmp file",'', '*.bmp')[0]
		self.ui.imageEdit.setText(self.imagePath)

	def progressBarCallback(self, args):
		self.updateProgressBar(args)

	def startButtonClicked(self):
		self.resetProgressBar()
		self.blockDimension = str(self.ui.dimensionEdit.text())
		print('Blocco: ', self.blockDimension, '\nPath: ', self.imagePath, '\nQuality: ', self.qualityValue)
		o = DCTAlg(self.imagePath, int(self.qualityValue), int(self.blockDimension))
		outImage = o.compress("USELESS_STRING", self.progressBarCallback)
		originalImage = PIL.Image.open(o.originalImgPath)
		self.showImage(originalImage, outImage)
		#print("\nDisplaying processed image")
		#outImage.show()
		#print("\nDisplaying original image")
		#originalImage.show()
	
	def resetProgressBar(self):
		self.ui.progressBar.reset()
		self.ui.progressBar.setRange(0,100)

	def updateProgressBar(self, value):
		self.progressBarValue = self.progressBar.value() + value
		self.ui.progressBar.setValue(self.progressBarValue)

	def showImage(self, originalImage, processedImage):
		tempDir = tempfile.gettempdir()
		tempOriginalFile = tempDir + "\\originalImage.bmp"
		tempProcessedFile = tempDir + "\\processedImage.bmp"
		misc.imsave(tempOriginalFile, originalImage)
		misc.imsave(tempProcessedFile, processedImage)
		os.system('explorer ' + tempOriginalFile)
		os.system('explorer ' + tempProcessedFile)

############################# MAIN #################################

if __name__ == "__main__":
	#suite = TestSuite()
	#suite.executeTests()
	if sys.platform == 'win32':
		os.environ["SCIPY_PIL_IMAGE_VIEWER"] = "explorer.exe"
	# Attiva lo scaling automatico dell'interfaccia
	os.environ["QT_AUTO_SCREEN_SCALE_FACTOR"] = "1"
	app = QApplication(sys.argv)
	dial = startDialog()
	sys.exit(app.exec_())
	