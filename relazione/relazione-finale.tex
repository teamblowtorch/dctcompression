\documentclass{article}

\usepackage[italian]{babel}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}

\usepackage{graphicx} % Required for the inclusion of images
\usepackage{natbib} % Required to change bibliography style to APA
\usepackage{amsmath, amsfonts, amsthm} % Required for some math elements 
\usepackage{hyperref}
\hypersetup{
     colorlinks   = true,
     citecolor    = blue
}
\usepackage{tabu}
\usepackage{float}
\usepackage{booktabs} % for \specialrule command
\usepackage{array}
\newcolumntype{?}{!{\vrule width 1pt}} % riga verticale in grassetto
\usepackage[export]{adjustbox} % Package per il cropping delle immagini
\usepackage{subfig} % sottofigure
\usepackage[a4paper]{geometry} 
\geometry{a4paper,tmargin=3.5cm, bmargin=2.5cm, lmargin=2.75cm, rmargin=2.75cm, headheight=3em, headsep=1.5cm, footskip=2cm}
\usepackage{lmodern} % Use latin modern font1
%\setlength\parindent{0pt} % Removes all indentation from paragraphs

\renewcommand{\labelenumi}{\alph{enumi}.} % Make numbering in the enumerate environment by letter rather than number (e.g. section 6)

%\usepackage{times} % Uncomment to use the Times New Roman font

%----------------------------------------------------------------------------------------
%	DOCUMENT INFORMATION
%----------------------------------------------------------------------------------------

\title{Metodi del calcolo scientifico \\ Relazione progetto 2 \\ Implementazione di Discrete Cosine Transform} % Title

\author{Darius \textsc{Sas} -- 764303 \\ Simone \textsc{Zini} -- 764800 } % Author name

\date{\today} % Date for the report

\begin{document}

\maketitle 

%-----------------------------------------------------------------------------------------------------------------
% INTRODUZIONE
%-----------------------------------------------------------------------------------------------------------------

\section{Introduzione}

La seguente relazione presenta un'implementantazione semplificata dell'algoritmo di compressione utilizzato da JPEG, al quale è stata aggiunta la possibilità di elaborare blocchi di dimensione $8N \times 8N$.
Nel seguito verrà documentato il codice del software sviluppato in Python, insieme a una breve descrizione dei metodi della libreria SciPy.

%-----------------------------------------------------------------------------------------------------------------
% COMPRESSIONE JPEG
%-----------------------------------------------------------------------------------------------------------------

\section{La compressione JPEG}

La compressione JPEG si basa sulla Discrete Cosine Transform ed è una sequenza di operazioni eseguite a partire dall'immagine originale su cui si vuole effettuare la compressione.
Elenchiamo di seguito i passaggi fondamentali dell'algoritmo di JPEG che sono stati implementati nel software sviluppato.
\\

Compressione:

\begin{itemize}

\item Lettura dell'immagine in toni di grigio;
\item Shift dei valori di intensità di -128;
\item Calcolo della DCT2 su blocchi di dimensione $8 \times 8$ pixels (il software permetterà di utilizzare blocchi $8N \times 8N$);
\item Divisione di ciascun blocco per la matrice di quantizzazione;
\item Arrotondamento del valori all'intero più vicino.

\end{itemize}

Decompressione:

\begin{itemize}

\item Moltiplicazione per la matrice di quantizzazione;
\item Esecuzione della IDCT2 sulla matrice;
\item Shift di +128 del risultato ottenuto dalla IDCT2;
\item Arrotondamento del valori all'intero più vicino;
\item Approssimazione del valori negativi a 0 e dei valori maggiori di 255 a 255 (Questo per poter rappresentare nuovamente la matrice come immagine in scala di grigi).

\end{itemize}

Di seguito, nella sezione \ref{sec:Python}, verranno mostrate le implementazioni e le estensioni di questi punti nel software sviluppato.

%-----------------------------------------------------------------------------------------------------------------
% SCIPY
%-----------------------------------------------------------------------------------------------------------------

\section{La libreria SciPy}

SciPy è una collezione di librerie open-source per calcoli matematici, scentifici ed elaborazione di immagini in ambiente Python.
I pacchetti che costituiscono SciPy sono:

\begin{center}
\begin{tabular}{r l}

\emph{NumPy} & package di base per array n-dimensionali;\\
\emph{SciPy Library} & libreria fondamentale per calcoli scientifici;\\
\emph{MatPlot2D} & libreria per il plotting di grafici bidimensionali;\\
\emph{IPython} & ambiente grafico interattivo;\\
\emph{Sympy} & libreria per la matematica simbolica;\\
\emph{Pandas} & libreria contenente strutture dati e metodi di analisi.\\

\end{tabular}
\end{center}

Di queste sono state utilizzate prevalentemente strutture dati contenute in NumPy (matrici e vettori) e i metodi \verb-dct(...)- e \verb-idct(...)- da SciPy.

Per la rappresentazione dell'immagine sotto forma di matrice e per le successive elaborazione si è deciso di utilizzare la struttura dati \verb-ndarray-: una struttura dati multidimensionale per memorizzare vettori e matrici.
Si è scelto di utilizzare questa struttura particolare rispetto a quelle standard di Python in quanto permette di lavorare solo alcune parti dell'array o della matrice; questo permette al software di compressione di effettuare i calcoli riguardanti i blocchi di $8N \times 8N$ pixel senza dover prima scomporre la matrice originale in piccole matrici, ma semplicemente lavorando sui range di valori interessati. Questa caratteristica riduce notevolmente l'uso di memoria, evitando di dover creare tante matrici fittizie, e ottimizzando i calcoli.

Per l'esecuzione dell'algoritmo DCT e IDCT sono state utilizzate le apposite funzioni di SciPy; la libreria non fornisce un metodo per la DCT2 (e di conseguenza nemmeno per la IDCT2), per cui è stato necessario elaborare la matrice prima sulle righe, poi sulle colonne, usando il metodo DCT (e IDCT).
SciPy offre 3 tipi di DCT tra gli 8 esistenti, in particolare è stata utilizzata la DCT di tipo 2, con normalizzazione \verb-"ortho"-. Per array \verb-x- unidimensionali la funzione \verb-dct(x, norm="ortho")- funziona come la DCT implementata in MATLAB.

%-----------------------------------------------------------------------------------------------------------------
% PYTHON DOC
%-----------------------------------------------------------------------------------------------------------------

\section{Compressione JPEG in Python}\label{sec:Python}
Per implementare l'algoritmo utilizzato da JPEG si è scelto di definire una classe \verb-DCTAlg- con i seguenti metodi, che ricalcano i passi esposti precedentemente:

\begin{itemize}
	\item \verb-def getImageMatrix(self, imagePath)-: Questo metodo prende in input il percorso dell'immagine nel file system e ritorna la matrice in toni di grigio dell'immagine indicata.
	\item \verb-def matrixComplementation(self)-: Questo metodo completa la matrice dell'immagine in modo tale che le dimensioni siano multipli di $8N$.
	\item \verb-def dctApply(self)-: Applica la DCT2 alla matrice che rappresenta l'immagine. La funzione applica a ciascun blocco di dimensione $8N\times 8N$ la funzione \verb-dct(...)- della libreria SciPy, alla vengono passati i vari blocchi della matrice dell'immagine due volte, la prima per il calcolo sulle righe e la seconda per il calcolo sulle colonne.
	\item \verb-def getExpandedQuantMatrix(self, quantizationMatrix)-: Questo metodo espande, e ritorna, la matrice di quantizzazione di default di JPEG in modo da poter essere usata su blocchi $8N\times 8N$.
	\item \verb-def adjustQuantMatrix(self, quality)-: Viene calcolata, e ritornata, la matrice di quantizzazione in base alla qualità indicata dal parametro \verb-quality-.
	\item \verb-def quantization(self)-: Applica la 'quantizzazione' a ciascun blocco $8N\times 8N$. Divide, arrotonda e moltiplica ciascun blocco della matrice dell'immagine per il rispettivo blocco della matrice di quantizzazione.
	\item \verb-def idctApply(self)-: Applica la DCT2 inversa, allo stesso modo di \verb-dctApply-.
	\item \verb-def normalizeImageMatrix(self)-: Modifica la matrice dell'immagine in modo che comprenda solamente valori appartenenti all'intervallo $[0, 255]$.
	\item \verb-def compress(self, outImagePath, callbackProgress)-: Applica nell'ordine corretto le precedenti operazioni e restituisce un oggetto di tipo PIL.Image a cui è stata applicata una compressione simil-JPEG. L'operazione di shift +128 (e successivametne di shift -128) viene eseguita da questo metodo.
\end{itemize}

La classe \verb-DCTAlg- viene richiamata tramite un'interfaccia grafica che permette all'utente di scegliere l'immagine che desidera manipolare e impostare il parametro $N$ ed il parametro di qualità.
Le immagini vengono successivamente visualizzate con il visualizzatore immagini predefinito del sistema.

%-----------------------------------------------------------------------------------------------------------------
% IMMAGINI
%-----------------------------------------------------------------------------------------------------------------

\section{Immagini a confronto}
% trim={<left> <lower> <right> <upper>}

In questa sezione vi è un confronto grafico della medesima immagine elaborata variando i parametri di qualità e di grandezza dei blocchi.
Si è scelto un riquadro dell'immagine che fosse il più significativo possibile, in modo da evidenziare le differenze tra le differenti qualità. Vi è anche un immagine compressa tramite l'algoritmo implementato in GIMP a qualità 30.

I valori di qualità dell'immagine ($Q$) e della dimensione dei blocchi ($N$) con i quali si è scelto di effettuare il test sono i seguenti rispettivamente: $\{30, 70, 100\}$ e $\{1, 4, 8\}$.

\begin{figure}[H]
\label{fig:confronto}
\caption{Confronto delle diverse immagini elaborate.}
	\centering
		\subfloat[Originale]{\adjincludegraphics[width=4cm,trim={20px {.62\height} {.80\width} 260px},clip]{./img/bridge/bridge.png}}
		\subfloat[Gimp con $Q=30$]{\adjincludegraphics[width=4cm,trim={20px {.62\height} {.80\width} 263px},clip]{./img/bridge/bridge_gimp30}}\\
	
		\subfloat[$Q=30$, $N=1$]{\adjincludegraphics[width=4cm,trim={20px {.62\height} {.80\width} 200px},clip]{./img/bridge/processedImage_30_1}}
		\subfloat[$Q=70$, $N=1$]{\adjincludegraphics[width=4cm,trim={20px {.62\height} {.80\width} 200px},clip]{./img/bridge/processedImage_70_1}}
		\subfloat[$Q=100$, $N=1$]{\adjincludegraphics[width=4cm,trim={20px {.62\height} {.80\width} 200px},clip]{./img/bridge/processedImage_100_1}}
	
		\subfloat[$Q=30$, $N=4$]{\adjincludegraphics[width=4cm,trim={20px {.62\height} {.80\width} 200px},clip]{./img/bridge/processedImage_30_4}}
		\subfloat[$Q=70$, $N=4$]{\adjincludegraphics[width=4cm,trim={20px {.62\height} {.80\width} 200px},clip]{./img/bridge/processedImage_70_4}}
		\subfloat[$Q=100$, $N=4$]{\adjincludegraphics[width=4cm,trim={20px {.62\height} {.80\width} 200px},clip]{./img/bridge/processedImage_100_4}}
		
		\subfloat[$Q=30$, $N=8$]{\adjincludegraphics[width=4cm,trim={20px {.62\height} {.80\width} 200px},clip]{./img/bridge/processedImage_30_8}}
		\subfloat[$Q=70$, $N=8$]{\adjincludegraphics[width=4cm,trim={20px {.62\height} {.80\width} 200px},clip]{./img/bridge/processedImage_70_8}}
		\subfloat[$Q=100$, $N=8$]{\adjincludegraphics[width=4cm,trim={20px {.62\height} {.80\width} 200px},clip]{./img/bridge/processedImage_100_8}}
	\end{figure}
%-----------------------------------------------------------------------------------------------------------------
% CONSIDERAZIONI FINALI
%-----------------------------------------------------------------------------------------------------------------

\section{Considerazioni finali}

Facendo un breve confronto con algoritmi presenti in altri programmi, come GIMP, si nota subito che la compressione ha un effetto diverso sulle stesse immagini, molto probabilmente per via della matrice di quantizzazione utilizzata da GIMP. Inoltre l'algoritmo scritto presenta alcune differenze dall'algoritmo effettivo di compressione JPEG.

\end{document}