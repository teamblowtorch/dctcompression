
def quantMatrixExp(q, n):
    
    qn = q
    row = 0

    while row < len(qn):
        
        i = 0

        #Duplica gli elementi sulle colonne
        while i < len(qn[row]):
            for j in range(0, n-1):
                qn[row].insert(i,qn[row][i])
            i += n

        #Duplica le righe
        for j in range(0, n-1):
            qn.insert(row,qn[row])
        row += n

    return qn